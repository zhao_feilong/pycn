import urllib.request
import re
import openpyxl


def geturl(url):
    r = urllib.request.Request(url)
    r.add_header('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15')
    page = urllib.request.urlopen(r)
    html = page.read().decode('utf-8')

    return html

def gets(html):
    #提取股票代码
    pp = re.compile('[S][HZ]\d\d\d\d\d\d[/]',re.S)
    #利用数组不能有重复项的性质删去重复项
    b = re.findall(pp, html)
    c = set(b)
    a = list(c)
    sorted(a,key = a.index)
    s = []
    #删去SH000001/SZ399001/SZ399006/
    for mmm in [i for i in range(len(a))]:
        if a[mmm] != 'SH000001/'and a[mmm] != 'SZ399001/'and a[mmm] !='SZ399006/':
            s.append(a[mmm])
    return s




def getm(html):
    #提取股票名称
    P = re.compile('"stock_price clearfix">.*?<h3>(.*?)</h3>', re.S)
    words0 = re.findall(P, html)
    P = re.compile('<dt.*?>(.*?)</dt>', re.S)
    words1 = re.findall(P, html)
    #提取数据
    P = re.compile( '<dd.*?>(.*?)</dd>', re.S)
    words2 = re.findall(P, html)
    print(words0)
    #print(words1)
    #print(words2)
    #筛选数据
    words2 = words2[0:18]
    #print(words2)
    words1 = words1[0:18]
    words1[14] = '市盈率'
    #print(words1)
    #可选择在运行窗口输出
    for i in range(len(words1)):
        word1 = words1[i]
        word2 = words2[i]
        print(word1,word2)
    #tt = ['股票名称及其代码']
    #tt.extend(words1)
    words0.extend(words2)
    #写入工作簿
    ws.append(words0)
if __name__ == '__main__':
    y = 'zq'
    count = 1
    while y == 'zq':

     #创建工作簿
     wb = openpyxl.Workbook()
     ws = wb.active
     ws.title = "选取的股票数据"
     ws.append(
        ['股票名称及其代码', '最高', '最低', '今开', '昨收', '涨停', '跌停', '换手率', '振幅', '成交量', '成交额', '内盘', '外盘', '量比', '涨跌幅', '市盈率',
         '市净率', '流通市值', '总市值']
        )
     #存储股票代码的url
     url1 = 'https://hq.gucheng.com/HSinfo.html?en_hq_type_code=&sort_field_name=px_change_rate&sort_type=desc&page='
     a = input("随机提取股票信息请按输入'1',提取预存股票请输入'2':")
     if a == '2':
        print("开始读取预先存储股票代码的当前信息")
        s = ['SH600778/', 'SZ002283/', 'SZ000599/', 'SH600178/']
        for num in [i for i in range(len(s))]:
            url = 'https://hq.gucheng.com/' + s[num]
            print(url)
            getm(geturl(url))
        print("数据提取完成，已存入D盘")
        wb.save('D:\指定股票代码数据.xlsx')
        y = 'cw'
     elif a == '1':
        print("开始随机读取沪深A股所有股票信息")
        for n in[i for i in range(5)]:
            url2 = 'https://hq.gucheng.com/HSinfo.html?en_hq_type_code=&sort_field_name=px_change_rate&sort_' \
                   'type=desc&page='+ str(n)
            s = gets(geturl(url2))
            for num in [i for i in range(len(s))]:
                url = 'https://hq.gucheng.com/' + s[num]
                print(url)
                getm(geturl(url))
        print("数据提取完成，已存入D盘")
        wb.save('D:\沪深A股数据.xlsx')
        y = 'cw'
     else:
        print("数字输入错误，请输入1或2")
        print("程序重新开始")
        y = 'zq'
        count = count + 1
if count > 1:
    print("请下次输入时注意1或2！")
print("程序结束")
